extends Node2D

onready var Move = get_node("../Move")
onready var Idle = get_node("../Idle")

func _ready():
	pass
	
func enter(actor):
	var pos_delta = null
	if Input.is_action_pressed("ui_up"):
		pos_delta = Vector2(0, -1)
	elif Input.is_action_pressed("ui_right"):
		pos_delta = Vector2(1, 0)
	elif Input.is_action_pressed("ui_down"):
		pos_delta = Vector2(0, 1)
	elif Input.is_action_pressed("ui_left"):
		pos_delta = Vector2(-1, 0)

	if pos_delta != null:
		actor.transition(Move, [pos_delta])
	else:
		actor.trasnistion(Idle)


func exit(actor):
	pass
