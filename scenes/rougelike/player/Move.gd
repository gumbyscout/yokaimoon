extends Node2D

onready var Idle = get_node("../Idle")
onready var Store = get_node("/root/Rougelike/Store")

var Actions = preload("../Actions.gd").new()

func _ready():
	pass

func enter(actor, pos_delta):
	#Check for collision
	actor.vision.cast_to = pos_delta * 64
	actor.vision.force_raycast_update()
	if actor.vision.is_colliding():
		var collider = actor.vision.get_collider()
		actor.transition(Idle, [false])
		
	else:
		$Sprite.show()
		var pos = actor.map.get_new_pos(actor, pos_delta)
		$Tween.interpolate_property(actor, "position", actor.position, pos, actor.MOVE_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		yield($Tween, "tween_completed")
		Store.dispatch(Actions.move(actor, pos))
		actor.transition(Idle)

func exit(actor):
	$Sprite.hide()
