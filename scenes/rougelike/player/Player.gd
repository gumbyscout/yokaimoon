extends Area2D

signal done

const MOVE_DURATION = .5 #seconds

export(NodePath) var _map_path
onready var map = get_node(_map_path)
onready var _state = $Idle
onready var vision = $Vision

func _ready():
	add_to_group("player")
	$Vision.add_exception(self)
	transition($Idle)

func act():
	self.transition($Act)
	return self
	
func transition(newState, args=[]):
	_state.exit(self)
	_state = newState
	_state.callv("enter", [self] + args)
