extends Node2D

func _ready():
	pass

func enter(actor, continue_turn = true):
	$Sprite.show()
	actor.emit_signal("done", continue_turn)

func exit(actor):
	$Sprite.hide()