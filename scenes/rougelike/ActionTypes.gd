extends Node

const MOVE = "MOVE"
const TURN_END = "TURN_END"
const TURN_START = "TURN_START"
const TURN_ABORT = "TURN_ABORT"
const COLLISION = "COLLISION"
