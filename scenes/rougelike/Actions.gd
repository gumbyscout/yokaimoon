extends Node

var types = preload("ActionTypes.gd")

func move(actor, pos):
	return {
		type = types.MOVE,
		msg = "%s moved to %s" % [actor, pos],
		actor = actor,
		pos = pos
	}
	
func turn_end():
	return {
		type = types.TURN_END,
		msg = "turn ended"
	}
	
func turn_startt():
	return {
		type = types.TURN_START,
		msg = "turn started"
	}
	
func turn_abort():
	return {
		type = types.TURN_ABORT,
		msg = "turn aborted"
	}
	
func collision(actor, collider):
	return {
		type = types.COLLISION,
		msg = "%s collided with %s" % [actor, collider],
		args = [actor, collider]
	}