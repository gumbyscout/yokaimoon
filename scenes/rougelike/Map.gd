extends TileMap

onready var _rouge_like = get_parent()

var _half_tile = get_cell_size() / 2

func _ready():
	pass

func get_new_pos(actor, pos_delta):
	# Do collision stuff here?
	var worldpos = actor.position
	var new_worldpos = map_to_world(world_to_map(worldpos) + pos_delta) + _half_tile	
	return new_worldpos

