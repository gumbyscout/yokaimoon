extends Node

onready var _actors = get_tree().get_nodes_in_group("actor")
onready var _player = get_tree().get_nodes_in_group("player")[0]

var Store = preload("res://lib/godot_redux/store.gd")
var Reducers = preload("Reducers.gd").new()
var Actions = preload("Actions.gd").new()

const _TURN_ACTIONS = ["ui_up", "ui_right", "ui_down", "ui_left", "ui_cancel", "ui_action"]

const TURN_DURATION = .5 

signal turn_end

onready var store = $Store
var reducers

func _ready():
	store.create([{
		name = "player",
		instance = Reducers
	}, {
		name = "game",
		instance = Reducers
	}])
	connect("turn_end", self, "_on_turn_end")
	
func _turn(continue_turn):
	_player.disconnect("done", self, "_turn")
	if continue_turn:
		for actor in _actors:
			actor.act()
			yield(actor, "done")
			
		store.dispatch(Actions.turn_end())
		emit_signal("turn_end")
		
	else:
		emit_signal("turn_end")

func _on_turn_end():
	set_physics_process(true)

func _physics_process(delta):
	for action in _TURN_ACTIONS:
		if Input.is_action_pressed(action):
			set_physics_process(false)
			_player.connect("done", self, "_turn")
			# Player moves first.
			_player.act()