extends Control

onready var _log = $Log/ScrollContainer/VBoxContainer/Label
onready var _store = get_node("../Store")

func _ready():
	_store.subscribe(self, "_state_changed")
	
func _state_changed(name, state):
	_log.set_text("state:\n %s" % _store.get())
	
