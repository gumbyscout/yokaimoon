extends Node

var types = preload("ActionTypes.gd")

func _shallow_copy(dict):
	return _shallow_merge(dict, {})

func _shallow_merge(src_dict, dest_dict):
	for i in src_dict.keys():
		dest_dict[i] = src_dict[i]
	return dest_dict

func player(state, action):
	match action.type:
		types.MOVE:
			var nextState = _shallow_copy(state)
			nextState["pos"] = action.pos
			return nextState
			
	return state
	
func game(state, action):
	match action.type:
		types.TURN_END:
			var nextState = _shallow_copy(state)
			var turns
			if not state.has("turns"):
				turns = 0
			else:
				turns = state["turns"]
				
			nextState["turns"] = turns + 1
			return nextState
			
	return state